package com.eduard.consoleReader;

import com.eduard.FlightException;
import com.eduard.controller.FlightController;

public interface ActionHandler {
   void doAction(Handler handler) throws FlightException;
}
