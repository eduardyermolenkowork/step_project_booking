package com.eduard;

public class FlightException extends Exception {
    public FlightException(String message){
        super(message);
    }
}
